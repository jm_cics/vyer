vy <- function(x, by=NULL, fun=length, out='each', sort=TRUE, byNA=TRUE, ...) {

  ########## Check Parameters

  if(! is.null(dim(x)) | is.null(x) | !is.atomic(x)) stop("x must be an atomic vector.")

  if(!is.list(by)) { if(!(is.atomic(by) | is.null(by))) stop("by must be a vector of values or a list.") }
  if(is.list(by)) { if(!all( sapply(by, is.atomic))) stop("elements of by are not all vectors.") }

  if(is.atomic(fun)) stop("fun must be a single function or list of functions.")
  if(is.list(fun)) { if( any(sapply(fun, is.atomic))) stop("All elements of fun list must be a single function.") }

  if(! is.logical(sort)) stop("sort must be logical.")
  if(length(sort) > 1) { sort <- sort[1]; warning("Only the first value of sort will be used.") }
  if(! is.logical(byNA)) stop("byNA must be logical.")
  if(length(byNA) > 1) { byNA <- byNA[1]; warning("Only the first value of byNA will be used.") }
  if(! is.character(out) | length(out) > 1) stop("out must be a character string: group, all, or each.") # any other value actually accepted as group

  ########## Prep Functions

  # First pull function names for later labeling

  if(is.list(fun)) {  # fun is supplied as a list:
    fun.is.fun <- FALSE

    fun.names <- if(is.null(names(fun))) { rep('', length(fun)) } else { names(fun) } # use names if any given
    fn.parts <- sapply(substitute(fun), deparse)[-1] # pull symbols from the argument
    fn.parts[substr(fn.parts, 1, 9) == 'function('] <- '' # don't use anonymous functions
    fun.names[fun.names=='' & fn.parts != ''] <- fn.parts[fun.names=='' & fn.parts != ''] # place symbols into names
    fun.names[fun.names == ''] <- paste('F', 1:length(fun), sep='')[fun.names == ''] # fill in the rest

  } else { # fun is not a list

    fun.is.fun <- TRUE
    fun.names <- if( length(substitute(fun)) == 1) deparse(substitute(fun)) else 'F1' # if a single symbol, use it
    fun <- list(fun) # wrap in a list

  }

  ########## Prep By (Groups)

  ### Pull by names if given for later labeling

  if(is.null(by)) {

    by.is.single <- TRUE
    bn.parts <- substitute(x)
    by.names <- if(is.symbol(bn.parts)) deparse(bn.parts) else 'V'
    by <- list(x)

  } else if(is.list(by)) {  # by is supplied as a list

    by.is.single <- FALSE

    by.names <- if(is.null(names(by))) { rep('', length(by)) } else { names(by) } # use names if any given

    bn.parts <- sapply(substitute(by), deparse) # pull text from the argument
    #bn.parts <- sapply(bn.parts, FUN=function(b) { rev(strsplit(b, split='$', fixed=TRUE)[[1]])[1] }) # deal with $ calls
    bn.parts[!sapply(substitute(by), is.symbol)] <- '' # remove non-symbols (e.g. derived values)
    bn.parts <- bn.parts[-1] # remove 'list()'
    by.names[by.names=='' & bn.parts != ''] <- bn.parts[by.names=='' & bn.parts != ''] # place symbols into names

    by.names[by.names == ''] <- paste('V', 1:length(by), sep='')[by.names == ''] # fill in the rest

  } else { # by is not a list (atomic)

    by.is.single <- TRUE
    by.names <- if( length(substitute(by)) == 1) deparse(substitute(by)) else 'V1' # if a single symbol, use it
    by <- list(by) # wrap in a list

  }

  ### Warn for Recycling/Truncating by

  for(i in 1:length(by)) { # for each by var

    if(length(by[[i]]) != length(x)) { # warn
      warning(
        if(by.is.single) 'by is a different length than x and will be truncated/recycled.'
        else paste('by element', by.names[i], 'is a different length than x and will be truncated/recycled.')
      )
    }

#    if(length(x) %% length(by[[i]]) != 0) { # check for uneven recycling
#      by[[i]] <- rep(by[[i]], length.out=length(x)) # and fix
#    }
  }


  ### Prepare groupings for iteration

  groups <- expand.grid(lapply(by, FUN=function(g) { # get combinations of variables
    if(sort==TRUE) sort(unique(g), na.last=TRUE) else unique(g)
  }))
  colnames(groups) <- by.names

  ########## Evaluation Loop

  result <- as.data.frame(matrix(NA, nrow=dim(groups)[1], ncol=length(fun)+1)) # empty df for each group and fun
  colnames(result) <- c(fun.names, '-N')

  for(g in 1:dim(groups)[1]) { # loop over each group (combination of vars)

    selector <- rep(TRUE, length(x)) # initialize group selector
    for(v in 1:dim(groups)[2]) { # check each variable...
      val <- by[[v]] # values to match
      mt <- groups[[g, v]] # match target

      selector <- selector & ((val == mt & !is.na(val) & !is.na(mt)) | (is.na(val) & is.na(mt))) # match values, or NAs, and combine with existing selection
    }

    result[g, '-N'] <- sum(selector) # record sample size
    if(sum(selector) > 0) { # for selectors with any elements, proceed
      for(f in 1:length(fun)) { # for every function...
        r <- fun[[f]](x[selector==TRUE], ...) # apply function to subset of x
        if(length(r)==0) browser()
        if(length(r) > 1) { # check for single value being returned; accept more than 1 if all identical; otherwise error:
          if(all(r == r[1])) r <- r[1] else stop(paste('function', fun.names[[f]], 'produced more than one value per group; should aggregate to a single result.'))
        }
        result[g, f] <- r # and store
      }
    }
  }

  ########## Export: labeling, reordering, assignment

  output <- cbind(groups, result) # combine group labels with calculations

  if(out != 'all') { output <- output[output$'-N' > 0, ] } # Except with 'all' option, remove unused grouping combinations
  output$'-N' <- NULL # strip count

  if(byNA == FALSE) { output <- output[rowSums(is.na(groups)) == 0, ] } # remove grouping NAs if desired

  if(out == 'item') { output <- merge(output, as.data.frame(by), by=1:length(by), sort=FALSE) } # For each, match output to original by

  if(fun.is.fun & by.is.single) { # Vector output if by and fun are single symbols (non-lists)
    vec.output <- output[, 2]
    names(vec.output) <- if(out != 'item') output[, 1] else NULL # name grouped output, but not each (none possible)
    return(vec.output)
  }

  rownames(output) <- NULL
  return(output)
}
